// ==UserScript==
// @name         Hide Dark Matter
// @namespace    ogameancients/hide-dark-matter
// @homepageURL  https://bitbucket.org/ogameancients/
// @icon64URL    https://bitbucket.org/ogameancients/ogame-helper-userscript/raw/master/resources/ogame-icon.jpg
// @updateURL    https://bitbucket.org/ogameancients/hide-dark-matter/raw/master/hide_dark_matter.user.js
// @downloadURL  https://bitbucket.org/ogameancients/hide-dark-matter/raw/master/hide_dark_matter.user.js
// @version      0.1.3
// @description  try to take over the world! Or at least OGame
// @author       Stefan Thoolen
// @match        https://*.ogame.gameforge.com/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    // Show only the free "found" amount of dark matter
    let tooltip = Tipped.get('#darkmatter_box').items()[0].content;
    let tableRows = tooltip.getElementsByTagName('tr');
    let foundAmount = tableRows[tableRows.length-1].cells[1].innerText;
    document.getElementById('resources_darkmatter').innerHTML = foundAmount;

    // Hide enabled officers on each page
    document.getElementById('officers').classList.remove('all');
    document.getElementById('officers').classList.remove('one');
    let officers = document.getElementById('officers').getElementsByClassName('tooltipHTML');
    for (let officerIndex = 0; officerIndex < officers.length; ++officerIndex) {
        officers[officerIndex].classList.remove('on');
        officers[officerIndex].classList.remove('shortTime');
    }

    // On the overview page, hide active items
    if (document.location.href.match('overview')) {
        document.getElementById('buffBar').getElementsByClassName('active_items')[0].style.display = 'none';
    }

    // On the resource center, we want to hide additional resources through officers and items
    if (document.location.href.match('resourceSettings')) {
        // Hide officers and look for items row
        let resourceTable = document.getElementsByClassName('listOfResourceSettingsPerPlanet')[0];
        var aboveOfficersTableRow = null;
        for (let resourceTableRow = 0; resourceTableRow < resourceTable.rows.length; ++resourceTableRow) {
            let officers = resourceTable.rows[resourceTableRow].getElementsByClassName('smallOfficer');
            if (officers.length <= 0) {
                continue;
            }
            if (aboveOfficersTableRow === null) {
                aboveOfficersTableRow = resourceTableRow - 1;
            }
            officers[0].className += ' grayscale';
            let resourceTableValues = resourceTable.rows[resourceTableRow].getElementsByClassName('tooltipCustom');
            for (let resourceTableValue = 0; resourceTableValue < resourceTableValues.length; ++resourceTableValue) {
                resourceTableValues[resourceTableValue].className += ' disabled';
            }
        }
        // Hide all items
        var itemsRow = resourceTable.rows[aboveOfficersTableRow];
        for (let itemsCol = 2; itemsCol < 6; ++itemsCol) {
            itemsRow.cells[itemsCol].className = 'normalmark';
            itemsRow.cells[itemsCol].innerHTML = '<span class="tooltipCustom" title="0">0</span>';
        }
    }

    // On the casino page, hide which officers are active
    if (document.location.href.match('page=premium')) {
        let officers = document.getElementById('building').getElementsByClassName('premium');
        for (let officerIndex = 0; officerIndex < officers.length; ++officerIndex) {
            let remaining = officers[officerIndex].getElementsByClassName('remaining');
            if (remaining.length > 0) {
                remaining[0].style.display = 'none';
            }
            let check = officers[officerIndex].getElementsByClassName('level');
            if (check.length > 0) {
                check[0].innerHTML = '<img src="https://gf3.geo.gfsrv.net/cdnbc/aa2ad16d1e00956f7dc8af8be3ca52.gif" width="12" height="11">';
            }
        }
    }
})();