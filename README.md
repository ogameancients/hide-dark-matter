# Hide dark matter in OGame

Install this script to hide your dark matter and active officers.
useful for making screenshots ;-)

## Installation instructions

1. Install Tampermonkey from https://tampermonkey.net/ (or any other userscript plugin)
2. Restart your webbrowser
3. Click [this button](https://bitbucket.org/ogameancients/hide-dark-matter/raw/master/hide_dark_matter.user.js) or add [hide_dark_matter.user.js](https://bitbucket.org/ogameancients/hide-dark-matter/raw/master/hide_dark_matter.user.js) as script manually.
